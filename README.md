##コードはReact.jsをメインに使っています。

###追記：nodeをインストールして置く必要があります。
https://qiita.com/akakuro43/items/600e7e4695588ab2958d

まずはターミナルを開き
ファイルをローカルに落とす
```
git clone https://seiya1001@bitbucket.org/seiya1001/amulive.git

または

git clone git@bitbucket.org:seiya1001/amulive.git

```

ファイルを落とせたらファイルの中に移動

```
cd amulive
```

cdnを使っていないのでReactの実行環境用パッケージのインストールのため以下のコマンドを入力してください。
すると、node_modulesというファイルが作られます。
```
npm install

```

##index.htmlを開くだけではjsが動かないのでlocalサーバーを起動する必要があります。
localサーバー起動
```
npm start

```

##本番環境用にビルド

本番環境用にデプロイするためのファイルは
まず以下のコマンドでsrcとpublic内のファイルがビルドされ
新たにbuildというフォルダが作られます。
その中のindex.htmlとstaticフォルダをサーバー上に置くことで見ることができます。
テストページはfirebaseにデプロイしています。


```
npm run build

```