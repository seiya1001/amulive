import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer>
      <div className="footer topFooter">
      <span className="menu"><a href="#">ご利用ガイド</a></span>
      <span className="menu"><a href="#">規約</a></span>
      <span className="menu"><a href="#">特定商取引に基づく表記</a></span>
      <span className="menu"><a href="#">会社概要</a></span>
      <span className="menu"><a href="#">???</a></span>
      <span className="menu endMenu"><a href="#">プライバシーポリシー</a></span>
      </div>
      <div className="footer underFooter">
      <div className="menu"><a href="#">FAQ</a></div>
      <div className="menu"><a href="#">サイトマップ</a></div>
      <div className="menu endMenu"><a href="#">お問い合わせ</a></div>
      </div>
      <span>アミュライブ</span>
      </footer>
      );
  }
}

