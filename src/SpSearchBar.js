import React, { Component } from 'react';
import './css/Accordion.css';
import ClassNames from 'classnames'
export default class SpSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state={
      isChecked: true,
      selectValue: '指定なし',
    }
  }

  checkButton = () =>{
    this.setState({isChecked: !this.state.isChecked})
  }

  onChangeSelectValue = (e) => {
    this.setState({selectValue: e.target.value})
  }


  render() {
    const classNames = ClassNames({
      checkButton: true,
      isChecked: this.state.isChecked,
    });
    return (
      <div className="spSearchContainer">
      <div className="spMenu listMenu">台を探す</div>
      <div className="spSerchTitle">マシン名称</div>
      <input type="text" name="search" value="" placeholder="遊技台検索" />

      <div className="spSerchTitle empty">
      空き台のみ
      <span className={classNames} onClick={this.checkButton}>
      <div className="checkmark_stem"></div>
      <div className="checkmark_kick"></div>
      </span>
      </div>

      <div className="spSerchTitle">種類</div>
      <select value={this.state.selectValue} onChange={this.onChangeSelectValue}>
      <option selected key="none">指定なし</option>
      <option key="pachinko">パチンコ</option>
      <option key="slot">スロット</option>
      </select>
      <div className="spSearchButton">
      検索
      <img alt="search" src={require('./img/SearchIconH.png')}/>
      </div>

      <div className="spMenu listMenu">人気ワード</div>
      <div className="tagMenuContainer">
      <div className="tagLine">
      <span className="tag">#oooo</span>
      <span className="tag">#oooo</span>
      <span className="tag">#oooo</span>
      </div>
      <div className="tagLine">
      <span className="tag">#oooo</span>
      <span className="tag">#oooo</span>
      <span className="tag">#oooo</span>
      </div>
      </div>
      </div>
      );
  }
}

// className="menu endMenu"