import React, { Component } from 'react'
import Slider from 'react-slick'
const SampleNextArrow = (props) => {
  return (
    <div
    className={"arrow"}
    style={{right: 0}}
    onClick={props.onClick}
    >&gt;</div>
    );
}

const SamplePrevArrow = (props) => {
  return (
    <div
    className={"arrow"}
    style={{ left:0}}
    onClick={props.onClick}
    >&lt;</div>
    );
}

export default class PauseOnHover extends Component {

  render() {
    var settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      className: "slider-wrapper",
    };
    return (
      <Slider {...settings}>
      <div><img alt="sample" className="sliderImage" src={require('./img/sample2.jpg')}/></div>
      <div><img alt="sample" className="sliderImage" src={require('./img/sample.jpg')}/></div>
        <div><img alt="sample" className="sliderImage" src={require('./img/sample2.jpg')}/></div>
      <div><img alt="sample" className="sliderImage" src={require('./img/sample.jpg')}/></div>
       <div><img alt="sample" className="sliderImage" src={require('./img/sample2.jpg')}/></div>
      <div><img alt="sample" className="sliderImage" src={require('./img/sample.jpg')}/></div>
      </Slider>
      );
  }
}