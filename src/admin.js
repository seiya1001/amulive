import React from 'react';
import firebase from 'firebase';
export default class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: '',
        password: '',
      },
    };
  };


  _send = () => {
    let { email, password } = this.state.data;
    firebase.auth().signInWithEmailAndPassword(email, password).then((result) => {
      alert("成功");
    }).catch(error => {
     if (error.code === 'auth/wrong-password') {
      alert('パスワードが違います');
    } else if(error.code ===  'auth/invalid-email') {
      alert('メールアドレスが違います');
    } else if(error.code ===  'auth/user-not-found') {
      alert('アカウントが存在しません');
    } else if(error.code ===  'auth/user-disabled') {
      alert('アカウントが無効です');
    }
  });
  }


  handleChange = (event) => {
        // ネストされたオブジェクトのdataまでアクセスしておく
        var data = this.state.data;
        // eventが発火したname属性名ごとに値を処理
        switch (event.target.name) {

          case 'email':
          data.email = event.target.value;
          break;

          case 'password':
          data.password = event.target.value;
          break;
        }
        // 状態を更新
        this.setState({
          data: data,
        });
      };

      render(){
        return(
          <div style={styles.container}>
          <div style={styles.form}>
          <label style={styles.label} >メールアドレス</label>
          <input type="email" name="email" value={this.state.email} onChange={this.handleChange} />
          </div>
          <div style={styles.form}>
          <label style={styles.label}>password</label>
          <input type="text" name="password" value={this.state.password} onChange={this.handleChange} />
          </div>
          <button style={styles.button} type="button" onClick={this._send}>送信</button>
          </div>
          );
      };
    };


    const styles = {
      container:{
       display: 'flex',
       flexDirection: 'column',
       padding: '30px',
       justifyContent: "center",
       alignItems: 'center',
     },
     form: {
      marginBottom: '20px',
    },
    label:{
      marginRight: '10px',
    },
    button: {
      width: '115px',
      height: '80px',
    }
  };