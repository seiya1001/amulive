import React, { Component } from 'react';
import '../css/App.css';
import ClassNames from 'classnames'
import '../css/drawer.css';
import SpMenuBar from '../SpMenuBar'
import SpSearchBar from '../SpSearchBar'
export default class SPHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: false,
    };
  }

  tabed = () => {
    const currentState = this.state.menu;
    this.setState({ menu: !currentState });
  };

  render() {
    const classNames = ClassNames({
      "activeDrawer": this.state.menu,
    });
    console.log(this.props.menu)
    return (
      <div className="header">
      <div className="nav-drawer" onClick={this.tabed}>
      <input id="nav-input" type="checkbox" className="nav-unshown"/>
      <label id="nav-open" htmlFor="nav-input"><span></span></label>
      <label className="nav-unshown" id="nav-close" htmlFor="nav-input"></label>
      <label className="nav-unshown" id="all" htmlFor="nav-input"></label>
      <div id="nav-content" className={classNames}>
      <SpMenuBar/>
      </div>
      </div>

      <div>
      <img alt="logo" src={require('../img/logo.png')}/>
      </div>

      <div className="nav-drawer">
      <input id="searchNav-input" type="checkbox" className="nav-unshown"/>
      <label htmlFor="searchNav-input">
      <img width="20" alt="search" src={require('../img/SpSearchIcon.png')}/>
      </label>

      <label className="nav-unshown" id="nav-close" htmlFor="searchNav-input"></label>
      <label className="nav-unshown" id="all" htmlFor="searchNav-input"></label>
      <div id="searchNav-content" className={classNames}>
      <SpSearchBar/>
      </div>
      </div>
      </div>
      );
  }
}
