import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
        <header className="App-header">
          <img alt="logo" src={require('../img/logo.jpg')}/>
        </header>
    );
  }
}

