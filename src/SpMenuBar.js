import React, { Component } from 'react';
import './css/Accordion.css';
export default class SpMenuBar extends Component {
  render() {
    return (
      <ul className="spMenuBar accbox">
      <li className="spMenu listMenu">メニュー</li>
      <li className="spMenu"><a href="#">ログイン/新規会員登録</a></li>
      <li className="spMenu"><a href="#">ログアウト</a></li>

      <input type="checkbox" id="label1" className="cssacc" />

      <label className="spMenu"for="label1">
      マイページ
      </label>
      <div className="accshow">

      <li><a href="#">プロフィール確認/設定</a></li>
      <li><a href="#">住所確認/設定</a></li>
      <li><a href="#">プレイ履歴</a></li>
      <li><a href="#">課金履歴</a></li>
      <li><a href="#">お問い合わせ</a></li>
      </div>

      <li className="spMenu"><a href="#">景品</a></li>
      <li className="spMenu"><a href="#">おすすめ台一覧</a></li>
      <li className="spMenu"><a href="#">ホール一覧</a></li>
      <li className="spMenu"><a href="#">台を探す</a></li>
      <li className="spMenu"><a href="#">予約状況</a></li>

      </ul>
      );
  }
}

// className="menu endMenu"