import React, { Component } from 'react';
import './css/dropDown.css';
export default class MenuBar extends Component {
  render() {
    return (
      <div className="menuContainer">
      <div className="menuBar">
      <div className="menu"><a href="#">おすすめ台一覧</a></div>
      <div className="menu"><a href="#">ホール一覧</a></div>
      <div className="menu"><a href="#">台を探す</a></div>
      <div className="menu"><a href="#">予約状況</a></div>
      <div className="menu"><a href="#">景品</a></div>

      <div className="menu endMenu">
      <span>5</span>
      マイページ
      <ul id="fade-in2" className="dropmenu">
      <li><a href="#">プロフィール確認/設定</a></li>
      <li><a href="#">住所確認/設定</a></li>
      <li><a href="#">プレイ履歴</a></li>
      <li><a href="#">課金履歴</a></li>
      <li><a href="#">お問い合わせ</a></li>
      </ul>
      </div>
      </div>
      </div>
      );
  }
}

// className="menu endMenu"