import React, { Component } from 'react';
import ClassNames from 'classnames'
export default class Ranking extends Component {
      constructor(props) {
            super(props);
            this.state ={
                  recommend: true,
                  pachinko: false,
                  slot: false,
            }
      }

      recommend = () =>{
            if(!this.state.recommend){
                  this.setState({
                        recommend: true,
                        pachinko: false,
                        slot: false,
                  })
            }
      }

      pachinko = () =>{
            if(!this.state.pachinko){
                  this.setState({
                        recommend: false,
                        pachinko: true,
                        slot: false,
                  })
            }
      }

      slot = () =>{
            if(!this.state.slot){
                  this.setState({
                        recommend: false,
                        pachinko: false,
                        slot: true,
                  })
            }
      }

      render() {
       const recommendClass = ClassNames({
          "rankTag": true,
          "active": this.state.recommend,
    });
       const pachinkoClass = ClassNames({
          "rankTag": true,
          "active": this.state.pachinko,
    });

       const slotClass = ClassNames({
          "rankTag": true,
          "active": this.state.slot,
    });

       return (
            <section className="rankingContainer">
            <div className="rankingTitle">
            総合ランキング
            </div>

            <div className="rankTagArea">
            <div className={recommendClass} onClick={this.recommend}>
            <img alt="sample" className="gameImage lamp" src={require('./img/lamp.png')}/>
            おすすめ台
            </div>
            <div className={pachinkoClass} onClick={this.pachinko}>
            <img alt="sample" className="gameImage" src={require("./img/machine2.png")}/>
            ぱちんこ
            </div>
            <div className={slotClass} onClick={this.slot}>
            <img alt="sample" className="gameImage" src={require("./img/machine.png")}/>
            スロット
            </div>
            </div>

            <div className="rankList">
            <div className="rankContent">
            <div className="rankText">
            <span className="rank">No.1</span><img alt="sample" className="cupImage" src={require('./img/gold.png')}/>
            </div>

            <div className="rankImage">
            <div className="rankImageFrame">
            <img alt="sample" width="100%" height="160"src={require('./img/sample.jpg')}/>
            </div>
            </div>
            <div className="gameName">
            機種名：　０００００
            </div>
            </div>

            <div className="rankContent">
            <div className="rankText">
            <span className="rank">No.2</span><img alt="sample" className="cupImage" src={require('./img/silver.png')}/>
            </div>
            <div className="rankImage">
            <div className="rankImageFrame">
            <img alt="sample" width="100%" height="160"src={require('./img/sample.jpg')}/>
            </div>
            </div>
            <div className="gameName">
            機種名：　０００００
            </div>
            </div>
            <div className="rankContent">
            <div className="rankText">
            <span className="rank">No.3</span><img alt="sample" className="cupImage" src={require('./img/bronze.png')}/>
            </div>
            <div className="rankImage">
            <div className="rankImageFrame">
            <img alt="sample" width="100%" height="160"src={require('./img/sample.jpg')}/>
            </div>
            </div>
            <div className="gameName">
            機種名：　０００００
            </div>
            </div>
            <div className="rankContent">
            <div className="rankText">
            <span className="rank">No.4</span>
            </div>
            <div className="rankImage">
            <div className="rankImageFrame">
            <img alt="sample" width="100%" height="160"src={require('./img/sample.jpg')}/>
            </div>
            </div>
            <div className="gameName">
            機種名：　０００００
            </div>
            </div>

            <div className="rankContent">
            <div className="rankText">
            <span className="rank">No.5</span>
            </div>
            <div className="rankImage">
            <div className="rankImageFrame">
            <img alt="sample" width="100%" height="160"src={require('./img/sample.jpg')}/>
            </div>
            </div>
            <div className="gameName">
            機種名：　０００００
            </div>
            </div>
            </div>
            </section>
            );
 }
}


