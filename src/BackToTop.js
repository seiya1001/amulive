import React, { Component } from 'react';

export default class BackToTop extends Component {
  scrollTop = () => {
    window.scrollTo(0, 0)
  }
  render() {
    return (
      <div className="page-top" onClick={this.scrollTop}>
      <span>&and;</span>
      <span>top</span>
      </div>
      );
  }
}




