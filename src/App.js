import React, { Component } from 'react';
import './css/App.css';
import './css/sp.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Header from './header/Header'
import SpHeader from './header/SpHeader'
import MenuBar from './MenuBar'
import SearchBar from './SearchBar'
import Slider from './Slider'
import Ranking from './Ranking'
import Footer from './Footer'
import SpFooter from './SpFooter'
import Main from './Main'
import BackToTop from './BackToTop'
import ClassNames from 'classnames'
import SpTag from './SpTag'
import AdminBar from './AdminBar'
import SpAdminBar from './SpAdminBar'
import firebase from 'firebase';
import Admin from './admin'
const width = window.innerWidth;
class App extends Component {
 constructor(props) {
  super(props);
  firebase.initializeApp({
 apiKey: "AIzaSyCm4uxlhFDpF4B2daUxsBZtpx_oPWx8EqE",
    authDomain: "amu-live.firebaseapp.com",
    databaseURL: "https://amu-live.firebaseio.com",
    projectId: "amu-live",
    storageBucket: "amu-live.appspot.com",
    messagingSenderId: "506261783080"
 });
  this.state = {
    logedIn: false,
    active: false,
  }
}

componentWillMount() {
  firebase.auth().onAuthStateChanged((user) => {
    console.log("app")
    if (user) {
     this.setState({
      logedIn: true,
    });
    // User is signed in.
  } else {
   this.setState({
    logedIn: false,
  });
    // No user is signed in.
  }
});
};


toggleClass = () => {
  console.log("toggleClass")
  const currentState = this.state.active;
  this.setState({ active: !currentState });
};

render() {
  const classNames = ClassNames({
    "App": true,
    "fixed": this.props.active,
  });
  if(this.state.logedIn){
    return (
      <div>
      <div className={classNames}>
      {width > 768 ?
       <div>
       <AdminBar/>
       <MenuBar/>
       <Header/>
       <SearchBar/>
       </div>
       :
       <div>
       <SpAdminBar/>
       <SpHeader onPress={() => this.toggleClass()} active={this.state.active}/>
       </div>
     }
     <Slider/>
     {width < 768 && <SpTag/>}
     <Ranking/>
     <Main/>
     <BackToTop/>
     {width > 768 ? <Footer/> : <SpFooter/>}
     </div>
     </div>
     );
  }else{
    return <Admin/>
  }
}
}

export default App;
